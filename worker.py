import logging
import json
import config

import rq
from redis import Redis
from time import sleep
from tornado import httpclient
from bs4 import BeautifulSoup
from utils import get_page_path, replace_uri_parts, count_word_freq
# from search import search_query

logger = logging.getLogger('msw_worker')
logger.setLevel(logging.DEBUG)


redis_connection = None
job_queue = None
msw_servers = None


def index_page(uri):
    client = httpclient.HTTPClient()
    response = client.fetch(uri)
    if response.error:
        logger.info(response.error)
        return

    page = BeautifulSoup(response.body)
    for s in page(['style', 'script', '[document]', 'head']):
        s.extract()

    word_freq = count_word_freq(page.get_text())
    logger.info(word_freq)
    json_string = json.dumps(dict(word_freq), separators=(',', ':'))
    page_path = get_page_path(uri)
    page_full_uri = config.MSW_SERVER_ADDR + page_path
    if redis_connection.get(config.REDIS_PAGE_NAVBLOCK_PREFIX + uri):
        # means that we've already have navblock for that page
        # TODO: add expiration for redis keys
        pass
    else:
        # need to add page to network
        candidates = []
        for server in msw_servers:

            if server == config.MSW_SERVER_ADDR:
                continue

            try:
                client = httpclient.HTTPClient()
                req = httpclient.HTTPRequest(server,
                                             method='MSW_SEARCH',
                                             body=json_string,
                                             allow_nonstandard_methods=True)
                res = client.fetch(req)
                candidates.extend([[w, p] for w, p in json.loads(res.body.decode())])
            except httpclient.HTTPError as e:
                logger.info("Can't fetch page: " + server + ': ' + str(e))

        top_candidates = sorted(candidates, key=lambda c: c[0], reverse=True)

        for _, cand_path in top_candidates[:5]:
            redis_connection.sadd(config.REDIS_PAGE_NAVBLOCK_PREFIX + page_path, cand_path)
            # TODO: remake for asynchronous http requests
            client = httpclient.HTTPClient()
            req = httpclient.HTTPRequest(cand_path,
                                         method='PUT_MSW_NAVIGATION_BLOCK',
                                         allow_nonstandard_methods=True,
                                         body=json.dumps({
                                             'pageUrl': page_full_uri
                                         }))

            client.fetch(req)

    redis_connection.set(config.REDIS_WORD_FREQ_PREFIX + get_page_path(uri), json_string)


def get_msw_servers():
    servers = redis_connection.get(config.REDIS_SERVERS_COLLECTION)
    servers = [key for key, v in json.loads(servers.decode()).items()]
    return servers


def run_worker():
    global redis_connection, job_queue, msw_servers
    redis_connection = Redis(host=config.REDIS_ADDR, port=config.REDIS_PORT)
    job_queue = rq.Queue(config.REDIS_INDEXING_QUEUE, connection=redis_connection)
    msw_servers = get_msw_servers()
    logger.info('starting worker')

    while True:
        job = job_queue.dequeue()
        if job:
            try:
                logger.debug('got job {0}'.format(job))
                uri = job.args[0]
                new_uri = replace_uri_parts(uri, netloc=config.WEB_SERVER_NETLOC)
                logger.info(new_uri)
                index_page(new_uri)
            except Exception as e:
                logger.error(e)
        else:
            # TODO remake for subscription to redis queue
            sleep(0.2)

logger.info(__name__)
if __name__ == '__main__':
    run_worker()
