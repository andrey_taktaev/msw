tornado>=4.2b1
rq>=0.5.2
redis>=2.10.3
nltk
BeautifulSoup4 >= 4.3.2