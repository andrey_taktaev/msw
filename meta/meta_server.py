import json
from urllib.parse import urlparse

import os.path
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import meta_config as config
import logging

from redis import Redis
from time import sleep
from tornado import httpclient
from bs4 import BeautifulSoup
from utils import get_page_path, replace_uri_parts, count_word_freq
import tornado.httpserver
import tornado.ioloop
import tornado.iostream
# import tornado.web
import tornado.httpclient
import tornado.escape
from tornado.web import url, RequestHandler, StaticFileHandler


redis_connection = None

logger = logging.getLogger('msw_meta_server')
logger.setLevel(logging.DEBUG)


class MSWMetaHandler(RequestHandler):
    SUPPORTED_METHODS = ('GET', 'POST', 'GET_MSW_SERVERS')

    def get(self):
        response = {}
        for server_url, server_info in redis_connection.hscan_iter(config.REDIS_SERVERS_COLLECTION):
            response[server_url.decode()] = json.loads(server_info.decode())

        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps(response))

    def get_msw_servers(self):
        self.get()

    def post(self):
        # TODO: add some auth
        server_info = json.loads(self.request.body.decode())
        try:
            _ = urlparse(server_info['mainPage'])
            redis_connection.hset(config.REDIS_SERVERS_COLLECTION, server_info['mainPage'], json.dumps(server_info))
            self.set_status(204)
        except KeyError as e:
            logger.error(str(e))
            self.set_status(400, 'bad data')

port = 8079

def run_server():
    global redis_connection
    redis_connection = Redis(host=config.REDIS_ADDR, port=config.REDIS_PORT)
    app = tornado.web.Application([
        url(r'/msw_servers', MSWMetaHandler),
        url(r'/(.*\.(html|js|css))', StaticFileHandler, {
            'path': os.path.join(os.path.dirname(__file__), "static")
        })
    ])
    app.listen(port)
    ioloop = tornado.ioloop.IOLoop.instance()
    ioloop.start()


if __name__ == "__main__":
    run_server()
