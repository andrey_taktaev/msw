from urllib.parse import urlparse, urlunparse
from nltk import word_tokenize, stem, FreqDist


def get_page_path(page_uri):
    # TODO: think of query parameters, need to normalize and count them for page ID
    return urlparse(page_uri).path


def replace_uri_parts(uri, **kwargs):
    new_uri = list(urlparse(uri))
    uri_components = ('scheme', 'netloc', 'path', 'params', 'query', 'fragment')
    for comp, index in zip(uri_components, range(len(uri_components))):
        if comp in kwargs:
            new_uri[index] = kwargs.get(comp)

    return urlunparse(new_uri)


def count_word_freq(text):
    lc_stemmer = stem.LancasterStemmer()
    stemmed_page = [lc_stemmer.stem(word) for word in word_tokenize(text)]
    word_freq = FreqDist(stemmed_page)
    for k, v in word_freq.items():
        word_freq[k] = word_freq.freq(k)
    return dict(word_freq)
