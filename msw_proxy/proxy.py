#!/usr/bin/env python

import logging
import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import rq
from redis import Redis
import config
from utils import replace_uri_parts, get_page_path
from search import msw_search, search_query
import json

import tornado.httpserver
import tornado.ioloop
import tornado.iostream
import tornado.web
import tornado.httpclient
import tornado.escape

logger = logging.getLogger('msw_proxy')
logger.setLevel(logging.DEBUG)

__all__ = ['run_proxy']


def fetch_request(url, callback, **kwargs):
    req = tornado.httpclient.HTTPRequest(url, **kwargs)
    client = tornado.httpclient.AsyncHTTPClient()
    client.fetch(req, callback)


redis_connection = None
job_queue = None

is_servers_fetched = False


class MSWProxyHandler(tornado.web.RequestHandler):
    MSW_METHODS = ('GET_MSW_NAVIGATION_BLOCK', 'PUT_MSW_NAVIGATION_BLOCK',
                   'GET_MSW_SERVERS', 'MSW_SEARCH', 'GET_MSW_WORD_FREQ')
    SUPPORTED_METHODS = tornado.web.RequestHandler.SUPPORTED_METHODS + MSW_METHODS

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        def common_req_handler(*fake_args, **fake_kwargs):
            pass

        for method in tornado.web.RequestHandler.SUPPORTED_METHODS:
            setattr(MSWProxyHandler, method.lower(), common_req_handler)

        if is_servers_fetched:
            return

    @tornado.web.asynchronous
    def prepare(self, *args, **kwargs):

        def handle_response(response):
            logger.debug(response)
            if (response.error and not
                    isinstance(response.error, tornado.httpclient.HTTPError)):
                self.set_status(500)
                self.write('Internal server error:\n' + str(response.error))
            else:
                self.set_status(response.code)
                for header in ('Date', 'Cache-Control', 'Server', 'Content-Type', 'Location'):
                    v = response.headers.get(header)
                    if v:
                        self.set_header(header, v)
                v = response.headers.get_list('Set-Cookie')
                if v:
                    for i in v:
                        self.add_header('Set-Cookie', i)
                if response.body:
                    self.write(response.body)
            content_type = response.headers.get('Content-Type')
            if content_type and ('text/html' in content_type or 'text/plain' in content_type):
                logger.debug('enqueuing job: {0}'.format(self.request.full_url()))
                job_queue.enqueue('msw_job', self.request.full_url(), timeout=3600)
            self.finish()

        if self.request.method not in self.MSW_METHODS:
            new_uri = replace_uri_parts(self.request.uri,
                                        scheme=config.WEB_SERVER_SCHEME,
                                        netloc=config.WEB_SERVER_NETLOC)
            logger.info('Requesting: {0}'.format(new_uri))
            self.request.headers['Host'] = config.WEB_SERVER_NETLOC
            fetch_request(new_uri, handle_response,
                          method=self.request.method, body=self.request.body,
                          headers=self.request.headers, follow_redirects=False,
                          allow_nonstandard_methods=True)

    def get_msw_navigation_block(self):
        self_page_path = get_page_path(self.request.full_url())
        pages = redis_connection.smembers(config.REDIS_PAGE_NAVBLOCK_PREFIX + self_page_path)
        pages = [page.decode() for page in pages]
        self.write(json.dumps(pages, separators=(',', ';')))
        self.finish()

    def put_msw_navigation_block(self):
        params = json.loads(self.request.body.decode())
        page_url = params['pageUrl']
        self_page_path = get_page_path(self.request.full_url())
        redis_connection.sadd(config.REDIS_PAGE_NAVBLOCK_PREFIX + self_page_path, page_url)
        self.set_status(204)
        self.finish()

    def msw_search(self):
        try:
            query = json.loads(self.request.body.decode())
        except ValueError:
            self.set_status(400)
            self.finish()
        else:
            try:
                search_results = msw_search(query)
                # top_five = list(itertools.islice(search_results, 0, 5))
                replace_len = len(config.REDIS_WORD_FREQ_PREFIX)
                self.write(json.dumps(search_results))
                self.finish()
            except Exception as e:
                logger.error(str(e))
                self.write('[]')
                self.finish()

    def get_msw_word_freq(self):
        logger.info('Getting msw word freq: {page}'.format(page=self.request.uri))
        word_freq = redis_connection.get(config.REDIS_WORD_FREQ_PREFIX + get_page_path(self.request.uri))
        if word_freq:
            self.write(word_freq)
        else:
            self.set_status(404)
        self.finish()


class SearchHandler(tornado.web.RequestHandler):
    SUPPORTED_METHODS = ('GET',)

    @tornado.web.asynchronous
    def get(self):
        query = self.get_argument('q')
        logger.info('Got search query: {query}'.format(query=query))
        results = search_query(query)
        self.write(json.dumps(results))
        self.finish()


def get_msw_servers():
    httpclient = tornado.httpclient.AsyncHTTPClient()
    add_self_req = tornado.httpclient.HTTPRequest(config.META_DATA_URI,
                                                  method='POST',
                                                  body=json.dumps({
                                                      'mainPage': config.MSW_SERVER_ADDR,
                                                      'description': config.MSW_SERVER_DESCRIPTION
                                                  }))
    httpclient.fetch(add_self_req)

    def save_msw_servers(response):
        if not response.error:
            global is_servers_fetched
            redis_connection.set(config.REDIS_SERVERS_COLLECTION, response.body)
            is_servers_fetched = True

    get_msw_servers_req = tornado.httpclient.HTTPRequest(config.META_DATA_URI,
                                                         method='GET_MSW_SERVERS',
                                                         allow_nonstandard_methods=True
                                                         )

    httpclient.fetch(get_msw_servers_req, save_msw_servers)


def run_proxy(port, start_ioloop=True):
    global redis_connection, job_queue
    redis_connection = Redis(host=config.REDIS_ADDR, port=config.REDIS_PORT)
    job_queue = rq.Queue(config.REDIS_INDEXING_QUEUE, connection=redis_connection)

    app = tornado.web.Application([
        (r'/ui/(.*\.(html|js|css))', tornado.web.StaticFileHandler, {
            'path': os.path.join(os.path.dirname(__file__), "static")
        }),
        (r'/search', SearchHandler),
        (r'.*', MSWProxyHandler)
    ])
    app.listen(port)
    ioloop = tornado.ioloop.IOLoop.instance()
    get_msw_servers()
    if start_ioloop:
        ioloop.start()

if __name__ == '__main__':
    port = 8888
    if len(sys.argv) > 1:
        port = int(sys.argv[1])

    print(("Starting MSW proxy on port %d" % port))
    run_proxy(port or int(sys.argv[1]))
