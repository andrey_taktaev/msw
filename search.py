from redis import Redis
import sys
import logging
import utils
import config
import json
import tornado.httpclient

from queue import PriorityQueue

logger = logging.getLogger('msw_search')
logger.setLevel(logging.DEBUG)

redis_connection = Redis(host=config.REDIS_ADDR, port=config.REDIS_PORT)


def cosine_metric(query, page):
    similarity = 0
    if len(query) > len(page):  # micro optimization
        page, query = query, page
    for k, v in query.items():
        similarity += page.get(k, 0) * v
    return similarity


def msw_search(query_word_freq):
    logger.debug('msw_search: {0}'.format(query_word_freq))
    pages = redis_connection.scan_iter(match=config.REDIS_WORD_FREQ_PREFIX + '*')
    ratings = PriorityQueue()
    visited_pages = set()
    self_addr = utils.replace_uri_parts(config.MSW_SERVER_ADDR, path='')
    # scan on our side
    for page in pages:
        visited_pages.add(self_addr + page.decode())
        p = redis_connection.get(page)
        page_word_freq = json.loads(p.decode())
        similarity = cosine_metric(query_word_freq, page_word_freq)
        if similarity != 0:
            logger.info('putting result {page}: {sim}'.format(page=page.decode(), sim=similarity))
            ratings.put((-similarity, page))

    logger.info(ratings)
    # replace_index = len(config.REDIS_WORD_FREQ_PREFIX)
    results = [(-sim, url.decode().replace(config.REDIS_WORD_FREQ_PREFIX, config.MSW_SERVER_ADDR)) for sim, url in ratings.queue[:5]]

    # search on other servers
    current_target = None
    httpclient = tornado.httpclient.HTTPClient()
    if not results:
        # current_target = config.MSW_SERVER_ADDR + ratings.queue[0][1].decode().replace(config.REDIS_WORD_FREQ_PREFIX, '')
        # else:
        msw_servers_data = redis_connection.get(config.REDIS_SERVERS_COLLECTION)
        msw_servers = json.loads(msw_servers_data.decode())
        for server in msw_servers:
            if server != config.MSW_SERVER_ADDR:
                req = tornado.httpclient.HTTPRequest(server,
                                                     method='MSW_SEARCH',
                                                     body=json.dumps(query_word_freq),
                                                     allow_nonstandard_methods=True)
                response = httpclient.fetch(req)
                search_results = json.loads(response.decode())
                if not search_results:
                    break

    other_candidates = []
    for page in results:
        # get page's navblock
        current_target = page[1]
        if self_addr in current_target:
            navblock = redis_connection.smembers(current_target.replace(config.MSW_SERVER_ADDR, config.REDIS_PAGE_NAVBLOCK_PREFIX))
        else:
            req = tornado.httpclient.HTTPRequest(current_target,
                                                 method='GET_MSW_NAVIGATION_BLOCK',
                                                 allow_nonstandard_methods=True)
            navblock_response = httpclient.fetch(req)
            navblock = json.loads(navblock_response.body.decode())

        steps = 0
        while steps < 10:
            most_close_page = (0, None)
            better_result = False
            for page in navblock:
                page = page.decode()
                req = tornado.httpclient.HTTPRequest(page,
                                                     method='GET_MSW_WORD_FREQ',
                                                     allow_nonstandard_methods=True)
                res = httpclient.fetch(req)
                page_freq = json.loads(res.decode())
                similarity = cosine_metric(query_word_freq, page_freq)
                if similarity > most_close_page[0]:
                    most_close_page = (similarity, page)
                    other_candidates.append(most_close_page)
            if most_close_page[1]:
                new_navblock_req = tornado.httpclient.HTTPRequest(page,
                                                                  method='GET_MSW_NAVIGATION_BLOCK',
                                                                  allow_nonstandard_methods=True)
                navblock_data = httpclient.fetch(new_navblock_req)
                navblock = json.loads(navblock_data.decode())
            else:
                break

        logger.debug('current results' + str(results))
        logger.debug('Other candidates' + str(other_candidates))

        # if len(results) > 100:
        #    break

    all_res = sorted(results, key=lambda page: page[0], reverse=True)
    fil_set = set()
    total = []
    for res in all_res:
        if res not in fil_set:
            fil_set.add(res)
            total.append(res)
    return total


def search_query(query):
    query_word_freq = utils.count_word_freq(query)
    return msw_search(query_word_freq)



if __name__ == "__main__":
    sys.exit()
